import { defineStore } from "pinia";
import authenticationService from '../../../services/authentication';
import { useRouter } from "vue-router";
import transactionService from "../../../services/transaction";
import { ref } from "vue";
import { useProgressCircularStore } from "../../../seedworks/progressCircularStore";

export const useAuthenticationStore = defineStore("handleAuthentication", () => {
  const router = useRouter();
  const loginError = ref(false);
  const errorMessage = ref('');
  const token = ref('');
  const userID = ref('');
  const login = ref({
    username: '',
    password: '',
  });

  async function submit() {
    try {
      useProgressCircularStore().circularOn();
      const response = await authenticationService.Authenticate(login.value.username, login.value.password);
      console.log(login.value.username+" "+login.value.password);
      token.value = response.data.token;
      userID.value = response.data.userID;
      localStorage.setItem('userToken', token.value); // จัดเก็บ token ใน localStorage
      localStorage.setItem('userId', userID.value); // จัดเก็บ userId ใน localStorage
      await transactionService.getTransactions(userID.value);
      router.push({
        name: 'selectaccount',
      });
      loginError.value = false;
      errorMessage.value = ''
      useProgressCircularStore().circularOff();
    } catch (error) {
      useProgressCircularStore().circularOff();
      loginError.value = true;
      errorMessage.value = 'username or password is wrong'
    }
  }
  function logout() {
    token.value = ''; // ล้างค่า token 
    localStorage.removeItem('userToken'); // ลบ token ออกจาก localStorage
    localStorage.removeItem('userId');
    localStorage.removeItem('accountNumber');
    localStorage.removeItem('bankID');
    router.push({ name: 'login' });
  }

  function selectAccount(account: Account) {
    router.push({
      name: 'transaction',
      query: {
        accountId: account.id,
        accountName: account.name,
      }
    });
  }

  function isAuthenticated() {
    const token = localStorage.getItem('userToken');
    if (!token) {
      return false;
    }
    try {
      // Decode token และตรวจสอบค่า exp
      const payload = JSON.parse(atob(token.split('.')[1]));
      const exp = payload.exp;
      const now = Date.now() / 1000; //เวลาปัจจุบัน เป็น UNIX timestamp
      // token หมดอายุ
      if (exp < now) {
        logout()
        return false;
      }
      return true;

    } catch (error) {
      return false;
    }
  }

  return {
    login,
    submit,
    router,
    token,
    logout,
    selectAccount,
    isAuthenticated,
    loginError,
    errorMessage
  }
});
