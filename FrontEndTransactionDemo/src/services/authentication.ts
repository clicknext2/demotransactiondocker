import http from "./axios";

async function Authenticate(username: string, password: string) {
    const loginData = {
        UserName: username,
        Password: password
    };

    try {
        var result = await http.post("Authentication/Authenticate", loginData, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return result;
    } catch (error) {
        console.error("Authentication error:", error);
        throw error;
    }
}

export default {
    Authenticate
};
