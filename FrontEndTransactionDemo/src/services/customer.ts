import http from "./axios";
async function getCustomers() {
    var result = await http.get("Customer/GetAllCustomer",{
        headers: {
          'Accept': 'application/json, text/plain, */*'
        }
      })
      return result;
}
export default{
    getCustomers
}
