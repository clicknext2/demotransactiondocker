import http from "./axios";
import {useProgressCircularStore} from "../seedworks/progressCircularStore";
async function getTransactions(userID: string) {
    useProgressCircularStore().circularOn();
      try {
        const payload = {
          userID: userID,
      };
        var result = await http.post("Transaction/GetTransactions", payload, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}` 
            }
        });
        useProgressCircularStore().circularOff();
        return result;
    } catch (error) {
      useProgressCircularStore().circularOff();
      throw error;
    }
}

async function deposit(amount: number) {
    useProgressCircularStore().circularOn();
    try {
        const accountNumber: string | null = localStorage.getItem('accountNumber') as string;
        const bankID: string | null = localStorage.getItem('bankID') as string;
      const payload = {
        amount: amount,
        fromAccountNumber: accountNumber,
        SenderBankID: parseInt(bankID)
    };
      var result = await http.post("Transaction/Deposit", payload, {
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('userToken')}` 
          }
      });
      useProgressCircularStore().circularOff();
      return result;
  } catch (error) {
    useProgressCircularStore().circularOff();
    throw error;
  }
}

async function withdraw(amount: number) {
    useProgressCircularStore().circularOn();
    try {
        const accountNumber: string | null = localStorage.getItem('accountNumber') as string;
        const bankID: string | null = localStorage.getItem('bankID') as string;
      const payload = {
        amount: amount,
        fromAccountNumber: accountNumber,
        SenderBankID: parseInt(bankID)
    };
      var result = await http.post("Transaction/Withdraw", payload, {
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('userToken')}` 
          }
      });
      useProgressCircularStore().circularOff();
      return result;
  } catch (error) {
    useProgressCircularStore().circularOff();
    throw error;
  }
}

async function transfer(request: TransferRequest) {
    useProgressCircularStore().circularOn();
    try {
        const accountNumber: string | null = localStorage.getItem('accountNumber') as string;
        const bankID: string | null = localStorage.getItem('bankID') as string;
      const payload = {
        receiverBankID: request.receiverBankID,
        amount: request.amount,
        toAccountNumber: request.toAccountNumber,
        fromAccountNumber: accountNumber,
        senderBankID: parseInt(bankID),
    };
      var result = await http.post("Transaction/Transfer", payload, {
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('userToken')}` 
          }
      });
      useProgressCircularStore().circularOff();
      return result;
  } catch (error) {
    useProgressCircularStore().circularOff();
    throw error;
  }
}
export default{
    getTransactions,
    transfer,
    deposit,
    withdraw
}
