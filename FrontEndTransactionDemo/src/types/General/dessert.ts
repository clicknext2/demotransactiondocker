export default interface Dessert  {
    name: string;
    calories: number;
  }
  