﻿using Microsoft.EntityFrameworkCore;
using TransactionDemo.Core.BusinessModels.Transaction;
using TransactionDemo.Core.Repositories.Account;
using TransactionDemo.Core.Repositories.User;
using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.Database.Models;
using TransactionDemo.SeedWorks;
using TransactionDemo.SeedWorks.Constants;
using TransactionDemo.SeedWorks.Repository;

namespace TransactionDemo.Core.BusinessLogics.Transaction
{
    public class TransactionManage
    {
        readonly IAccountRepository accountRepository;
        readonly ITransactionRepository transactionRepository;
        readonly TransactionDemoContext _context;

        public TransactionManage(IAccountRepository accountRepository
            , ITransactionRepository transactionRepository
            , TransactionDemoContext context
            )
        {
            this.accountRepository = accountRepository;
            this.transactionRepository = transactionRepository;
            _context = context;
        }

        public async Task<ResultApi<bool>> Deposit(TransactionRequest request)
        {
            return await CreateTransaction(request, TransactionTypeID.Deposit);
        }

        public async Task<ResultApi<bool>> Withdraw(TransactionRequest request)
        {
            return await CreateTransaction(request, TransactionTypeID.Withdraw);
        }

        public async Task<ResultApi<bool>> Transfer(TransactionRequest request)
        {
            return await CreateTransaction(request, TransactionTypeID.Transfer);
        }

        private async Task<ResultApi<bool>> CreateTransaction(TransactionRequest request, TransactionTypeID transactionType)
        {
            try
            {
                var recipientAccount = new AccountModel();
                if (request.Amount <= 0)
                    throw new ArgumentException("Please enter a valid amount");

                var acccount = await (from a in accountRepository.Queryable
                                      where a.AccountNumber == request.FromAccountNumber
                                         && a.BankId == request.SenderBankID
                                      select a).SingleOrDefaultAsync();
                if (acccount == null)
                    throw new ArgumentException("From account number is invalid");

                if (transactionType == TransactionTypeID.Withdraw)
                {
                    if (acccount.Balance < request.Amount)
                        throw new ArgumentException("Insufficient funds in the account");
                }

                if (transactionType == TransactionTypeID.Transfer)
                {
                    recipientAccount = await (from a in accountRepository.Queryable
                                              where a.AccountNumber == request.ToAccountNumber
                                                 && a.BankId == request.ReceiverBankID
                                              select a).SingleOrDefaultAsync();

                    if (recipientAccount == null)
                        throw new ArgumentException("To account number or receiver bank invalid");

                    if (acccount.Balance < request.Amount)
                        throw new ArgumentException("Insufficient funds in the account");

                    if (request.FromAccountNumber == request.ToAccountNumber)
                        throw new ArgumentException("Account number reciver is the same");
                }

                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        var balanceRequest = request.Amount;
                        var existingBalance = acccount.Balance;

                        if (transactionType == TransactionTypeID.Deposit)
                        {
                            var updateBalance = balanceRequest + existingBalance;
                            await accountRepository.Update(new SingleKeyUpdateCommand<AccountModel>
                            {
                                Id = acccount.AccountId,
                                Modifications = new List<FieldModification<AccountModel>>()
                            {
                                FieldModification<AccountModel>.Create(x => x.Balance, updateBalance),
                            }});

                            await transactionRepository.Insert(new TransactionModel
                            {
                                TransactionId = Guid.NewGuid(),
                                Amount = request.Amount,
                                FromAccountId = acccount.AccountId,
                                Type = TransactionTypeID.Deposit,
                                CreatedAt = DateTime.Now,
                            });
                        }
                        else if (transactionType == TransactionTypeID.Withdraw)
                        {
                            var updateBalance = existingBalance - balanceRequest;
                            await accountRepository.Update(new SingleKeyUpdateCommand<AccountModel>
                            {
                                Id = acccount.AccountId,
                                Modifications = new List<FieldModification<AccountModel>>()
                            {
                                 FieldModification<AccountModel>.Create(x => x.Balance, updateBalance),
                            }});

                            await transactionRepository.Insert(new TransactionModel
                            {
                                TransactionId = Guid.NewGuid(),
                                Amount = request.Amount,
                                FromAccountId = acccount.AccountId,
                                Type = TransactionTypeID.Withdraw,
                                CreatedAt = DateTime.Now,
                            });
                        }
                        else if (transactionType == TransactionTypeID.Transfer)
                        {
                            var updateBalance = existingBalance - balanceRequest;
                            await accountRepository.Update(new SingleKeyUpdateCommand<AccountModel>
                            {
                                Id = acccount.AccountId,
                                Modifications = new List<FieldModification<AccountModel>>()
                            {
                                FieldModification<AccountModel>.Create(x => x.Balance, updateBalance),
                            }
                            });

                            var updateBalanceRecipientAccount = balanceRequest + recipientAccount.Balance;
                            await accountRepository.Update(new SingleKeyUpdateCommand<AccountModel>
                            {
                                Id = recipientAccount.AccountId,
                                Modifications = new List<FieldModification<AccountModel>>()
                            {
                                FieldModification<AccountModel>.Create(x => x.Balance, updateBalanceRecipientAccount),
                            }
                            });

                            await transactionRepository.Insert(new TransactionModel
                            {
                                TransactionId = Guid.NewGuid(),
                                Amount = request.Amount,
                                FromAccountId = acccount.AccountId,
                                ToAccountId = recipientAccount.AccountId,
                                Type = TransactionTypeID.Transfer,
                                CreatedAt = DateTime.Now,
                            });

                            await transactionRepository.Insert(new TransactionModel
                            {
                                TransactionId = Guid.NewGuid(),
                                Amount = request.Amount,
                                FromAccountId = acccount.AccountId,
                                ToAccountId = recipientAccount.AccountId,
                                Type = TransactionTypeID.Recieve,
                                CreatedAt = DateTime.Now,
                            });
                        }
                        _context.SaveChanges();
                        await dbContextTransaction.CommitAsync();
                        return new ResultApi<bool> { Message = "Success", StatusCode = 200, Data = true };
                    }
                    catch 
                    {
                        await dbContextTransaction.RollbackAsync();
                        throw;
                    }
                }
            }
            catch (ArgumentException ex)
            {
                return new ResultApi<bool> { Message = ex.Message, StatusCode = 400 };
            }
            catch (Exception ex)
            {
                return new ResultApi<bool> { Message = ex.Message, StatusCode = 500 };
            }
        }
    }
}
