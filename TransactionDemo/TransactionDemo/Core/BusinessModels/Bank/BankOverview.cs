﻿namespace TransactionDemo.Core.BusinessModels.Bank
{
    public class BankOverviewResponse
    {
        public int BankID { get; set; }
        public string Name { get; set; } = null!;
    }
}
