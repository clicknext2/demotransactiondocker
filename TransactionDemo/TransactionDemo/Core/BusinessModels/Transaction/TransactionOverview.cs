﻿using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.BusinessModels.Transaction
{
    public class TransactionOverviewResponse
    {
        public Guid AccountID { get; set; }
        public string AccountNumber { get; set; } = null!;
        public string AccountName { get; set; } = null!;
        public int BankID { get; set; }
        public string BankName { get; set; } = null!;
        public decimal Remain { get; set; }
        public List<TransactionTransferResponse>? Transactions { get; set; } 
    }

    public class TransactionTransferResponse
    {
        public Guid TransactionID { get; set; }
        public DateTime Date { get; set; }
        public TransactionTypeID TransactionType { get; set; }
        public decimal Amount { get; set; }
        public string FromAccountName { get; set; } = null!;
        public string? ToAccountName { get; set; }
    }
    public class TransactionOverviewRequest
    {
        public Guid UserID { get; set; }
    }
}
