﻿using Microsoft.EntityFrameworkCore;
using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.Database.Models;
using TransactionDemo.SeedWorks.Repository;

namespace TransactionDemo.Core.Repositories.Account
{
    public class AccountRepository : IAccountRepository
    {
        static UpdateMetadata<AccountModel, Database.Models.Account> updateMetadata = new UpdateMetadata<AccountModel, Database.Models.Account>()
            .Add(x => x.AccountId, x => x.AccountId)
            .Add(x => x.UserId, x => x.UserId)
            .Add(x => x.Balance, x => x.Balance)
            .Add(x => x.Name, x => x.Name)
            .Add(x => x.AccountNumber, x => x.AccountNumber)
            .Add(x => x.BankId, x => x.BankId)
           ;

        readonly TransactionDemoContext _context;

        public AccountRepository(TransactionDemoContext context)
        {
            _context = context;
        }
        public IQueryable<AccountModel> Queryable => _context.Accounts.Select(x => new AccountModel
        {
            AccountId = x.AccountId,
            AccountNumber = x.AccountNumber,
            Balance = x.Balance,
            Name = x.Name,
            UserId = x.UserId,
            BankId = x.BankId
        });

        public async Task Update(SingleKeyUpdateCommand<AccountModel> command)
        {
            var entity = await _context.Accounts.SingleAsync(x => x.AccountId == command.Id);
            var entityEntry = _context.Entry(entity);

            foreach (var modification in command.Modifications)
            {
                var metadata = updateMetadata[modification.FieldName];
                entityEntry.Property(metadata.EntityModelPropertyName).CurrentValue = metadata.Converter(modification.FieldValue);
            }
        }
    }
}
