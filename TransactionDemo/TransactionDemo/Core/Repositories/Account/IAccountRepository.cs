﻿using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.SeedWorks.Repository;

namespace TransactionDemo.Core.Repositories.Account
{
    public interface IAccountRepository
    {
        IQueryable<AccountModel> Queryable { get; }
        Task Update(SingleKeyUpdateCommand<AccountModel> command);
    }
}
