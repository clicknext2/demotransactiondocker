﻿using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.Database.Models;
using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.Repositories.Bank
{
    public class BankRepository : IBankRepository
    {
        readonly TransactionDemoContext _context;

        public BankRepository(TransactionDemoContext context)
        {
            _context = context;
        }
        public IQueryable<BankModel> Queryable => _context.Banks.Select(x => new BankModel
        {
            BankId = x.BankId,
            Name = x.Name,
        });
    }
}
