﻿using TransactionDemo.Core.RepositoryModels;

namespace TransactionDemo.Core.Repositories.User
{
    public interface ITransactionRepository
    {
        IQueryable<TransactionModel> Queryable { get; }
        Task Insert(TransactionModel model);
    }
}
