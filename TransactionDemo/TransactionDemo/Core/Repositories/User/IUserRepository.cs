﻿using TransactionDemo.Core.RepositoryModels;

namespace TransactionDemo.Core.Repositories.User
{
    public interface IUserRepository
    {
        IQueryable<UserModel> Queryable { get; }
    }
}
