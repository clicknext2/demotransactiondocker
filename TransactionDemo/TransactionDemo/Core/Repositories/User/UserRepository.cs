﻿using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.Database.Models;

namespace TransactionDemo.Core.Repositories.User
{
    public class UserRepository : IUserRepository
    {
        readonly TransactionDemoContext _context;

        public UserRepository(TransactionDemoContext context)
        {
            _context = context;
        }

        public IQueryable<UserModel> Queryable => _context.Users.Select(x => new UserModel
        {
            UserId = x.UserId,
            Name = x.Name,
            Password = x.Password,
            Username = x.Username
        });
    }
}
