﻿namespace TransactionDemo.Core.RepositoryModels
{
    public class AccountModel
    {
        public Guid AccountId { get; set; }
        public string AccountNumber { get; set; } = null!;
        public string Name { get; set; } = null!;
        public Guid UserId { get; set; }
        public decimal Balance { get; set; }
        public int BankId { get; set; }
    }
}
