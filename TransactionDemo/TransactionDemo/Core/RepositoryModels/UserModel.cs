﻿namespace TransactionDemo.Core.RepositoryModels
{
    public class UserModel
    {
        public Guid UserId { get; set; }
        public string Name { get; set; } = null!;
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
