﻿using System;
using System.Collections.Generic;

namespace TransactionDemo.Database.Models
{
    public partial class Account
    {
        public Account()
        {
            TransactionFromAccounts = new HashSet<Transaction>();
            TransactionToAccounts = new HashSet<Transaction>();
        }

        public Guid AccountId { get; set; }
        public string AccountNumber { get; set; } = null!;
        public string Name { get; set; } = null!;
        public decimal Balance { get; set; }
        public Guid UserId { get; set; }
        public int BankId { get; set; }

        public virtual Bank Bank { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<Transaction> TransactionFromAccounts { get; set; }
        public virtual ICollection<Transaction> TransactionToAccounts { get; set; }
    }
}
