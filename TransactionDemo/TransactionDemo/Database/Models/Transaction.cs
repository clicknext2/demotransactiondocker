﻿using System;
using System.Collections.Generic;

namespace TransactionDemo.Database.Models
{
    public partial class Transaction
    {
        public Guid TransactionId { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public Guid FromAccountId { get; set; }
        public Guid? ToAccountId { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual Account FromAccount { get; set; } = null!;
        public virtual Account? ToAccount { get; set; }
    }
}
