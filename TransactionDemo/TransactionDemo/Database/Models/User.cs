﻿using System;
using System.Collections.Generic;

namespace TransactionDemo.Database.Models
{
    public partial class User
    {
        public User()
        {
            Accounts = new HashSet<Account>();
        }

        public Guid UserId { get; set; }
        public string Name { get; set; } = null!;
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;

        public virtual ICollection<Account> Accounts { get; set; }
    }
}
