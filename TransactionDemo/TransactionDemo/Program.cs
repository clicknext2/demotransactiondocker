using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using TransactionDemo.Core.BusinessLogics.Authentication;
using TransactionDemo.Core.BusinessLogics.Bank;
using TransactionDemo.Core.BusinessLogics.Transaction;
using TransactionDemo.Core.Repositories.Account;
using TransactionDemo.Core.Repositories.Bank;
using TransactionDemo.Core.Repositories.Transaction;
using TransactionDemo.Core.Repositories.User;
using TransactionDemo.Database.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IAccountRepository, AccountRepository>();
builder.Services.AddScoped<ITransactionRepository, TransactionRepository>();
builder.Services.AddScoped<IBankRepository, BankRepository>();
builder.Services.AddScoped<AuthenticationManage>();
builder.Services.AddScoped<TransactionManage>();
builder.Services.AddScoped<TransactionOverview>(); 
    builder.Services.AddScoped<BankOverview>();
builder.Services.AddDbContext<TransactionDemoContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)

            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(builder.Configuration["Jwt:Key"])),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        builder => builder.WithOrigins("http://localhost:3000") // Adjust with your frontend URL
                          .AllowAnyHeader()
                          .AllowAnyMethod());
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthentication(); // ?????? Authentication Middleware
app.UseAuthorization(); // ?????? Authorization Middleware

app.UseCors("AllowSpecificOrigin"); // Use CORS with the specified policy

app.MapControllers();

app.Run();
