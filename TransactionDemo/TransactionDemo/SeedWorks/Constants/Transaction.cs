﻿namespace TransactionDemo.SeedWorks.Constants
{
    public enum TransactionTypeID
    {
        Deposit = 1,  // ฝาก
        Withdraw = 2, // ถอน
        Transfer = 3, // โอน
        Recieve = 4, // รับ
    }

}
