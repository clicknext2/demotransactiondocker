﻿using System.Linq.Expressions;

namespace TransactionDemo.SeedWorks.Repository
{
    public class FieldModification<T>
    {
        public string FieldName { get; set; }
        public object FieldValue { get; set; }

        private FieldModification() { }

        public static FieldModification<T> Create<TField>(Expression<Func<T, TField>> propertyExpression, object value)
        {
            var memberExpression = (MemberExpression)propertyExpression.Body;
            var propertyName = memberExpression.Member.Name;

            return new FieldModification<T>
            {
                FieldName = propertyName,
                FieldValue = value,
            };
        }
    }
}
