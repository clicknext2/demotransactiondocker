﻿namespace TransactionDemo.SeedWorks.Repository
{
    public abstract class UpdateCommand<T>
    {
        public IEnumerable<FieldModification<T>> Modifications { get; set; }
    }
}
