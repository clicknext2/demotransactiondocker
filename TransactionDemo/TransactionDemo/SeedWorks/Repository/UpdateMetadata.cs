﻿using System.Linq.Expressions;

namespace TransactionDemo.SeedWorks.Repository
{
    public class UpdateMetadata<TRepositoryModel, TEntityModel> : Dictionary<string, UpdateMetadataItem<TEntityModel>>
    {
        public UpdateMetadata<TRepositoryModel, TEntityModel> Add<TRepositoryModelMember, TEntityModelMember>(
              Expression<Func<TRepositoryModel, TRepositoryModelMember>> repositoryModelPropertyExpresion
            , Expression<Func<TEntityModel, TEntityModelMember>> entityModelPropertyExpresion
            )
        {
            return Add(repositoryModelPropertyExpresion, entityModelPropertyExpresion, x => x);
        }

        public UpdateMetadata<TRepositoryModel, TEntityModel> Add<TRepositoryModelMember, TEntityModelMember>(
              Expression<Func<TRepositoryModel, TRepositoryModelMember>> repositoryModelPropertyExpresion
            , Expression<Func<TEntityModel, TEntityModelMember>> entityModelPropertyExpresion
            , Func<object, object> converter
            )
        {
            var repositoryModelPropertyName = GetPropertyName(repositoryModelPropertyExpresion);
            var entityModelPropertyName = GetPropertyName(entityModelPropertyExpresion);

            Add(repositoryModelPropertyName, new UpdateMetadataItem<TEntityModel>
            {
                RepositoryModelPropertyName = repositoryModelPropertyName,
                EntityModelPropertyName = entityModelPropertyName,
                Converter = converter,
            });

            return this;
        }

        public string GetPropertyName<TModel, TMember>(Expression<Func<TModel, TMember>> propertyExpresion)
        {
            var memberExpression = (MemberExpression)propertyExpresion.Body;
            return memberExpression.Member.Name;
        }
    }
}
