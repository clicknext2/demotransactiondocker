﻿using Microsoft.AspNetCore.Mvc;
using TransactionDemo.Core.BusinessLogics.Authentication;

namespace TransactionDemo.Services.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        readonly AuthenticationManage authenticationManage;

        public AuthenticationController(AuthenticationManage authenticationManage)
        {
            this.authenticationManage = authenticationManage;
        }

        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] StandardLoginRequest request)
        {
            var result = await authenticationManage.Authentication(request.UserName, request.Password);
            if (result.token == null)
                return Unauthorized();

            return Ok(new { Token = result.token, UserID = result.userId});
        }

    }
    public class StandardLoginRequest
    {
        public string UserName { get; set; } = null!;
        public string Password { get; set; } = null!;
    }
}
