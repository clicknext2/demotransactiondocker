﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TransactionDemo.Core.BusinessLogics.Bank;
using TransactionDemo.Core.BusinessModels.Bank;
using TransactionDemo.SeedWorks;

namespace TransactionDemo.Services.Controllers
{
    [Route("[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class MasterDataController : ControllerBase
    {
        readonly BankOverview bankOverview;

        public MasterDataController(BankOverview bankOverview)
        {
            this.bankOverview = bankOverview;
        }

        [HttpGet]
        public async Task<ResultApi<List<BankOverviewResponse>>> GetBanks()
        {
            var result = await bankOverview.GetBanks();
            return result;
        }
    }
}
