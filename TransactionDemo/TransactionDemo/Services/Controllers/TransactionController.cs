﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TransactionDemo.Core.BusinessLogics.Transaction;
using TransactionDemo.Core.BusinessModels.Transaction;
using TransactionDemo.SeedWorks;

namespace TransactionDemo.Services.Controllers
{
    [Route("[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        readonly TransactionManage transactionManage;
        readonly TransactionOverview transactionOverview;

        public TransactionController(TransactionManage transactionManage, TransactionOverview transactionOverview)
        {
            this.transactionManage = transactionManage;
            this.transactionOverview = transactionOverview;
        }

        [HttpPost]
        public async Task<ResultApi<List<TransactionOverviewResponse>>> GetTransactions([FromBody] TransactionOverviewRequest request)
        {
            var result = await transactionOverview.GetTransactions(request);
            return result;
        }

        [HttpPost]
        public async Task<ResultApi<bool>> Deposit([FromBody] TransactionRequest request)
        {
            var result = await transactionManage.Deposit(request);
            return result;
        }

        [HttpPost]
        public async Task<ResultApi<bool>> Withdraw([FromBody] TransactionRequest request)
        {
            var result = await transactionManage.Withdraw(request);
            return result;
        }

        [HttpPost]
        public async Task<ResultApi<bool>> Transfer([FromBody] TransactionRequest request)
        {
            var result = await transactionManage.Transfer(request);
            return result;
        }
    }
}
