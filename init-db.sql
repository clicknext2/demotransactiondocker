IF db_id('TransactionDemo') IS NOT NULL
BEGIN
    PRINT 'Database TransactionDemo already exists';
    PRINT 'Dropping existing Database TransactionDemo...';
    DROP DATABASE [TransactionDemo];
    PRINT 'Database TransactionDemo dropped';
END

PRINT 'Creating Database TransactionDemo';
CREATE DATABASE [TransactionDemo];
PRINT 'Database TransactionDemo created';
GO

USE [TransactionDemo];
PRINT 'Creating tables...';
GO

-- Create tables
CREATE TABLE [dbo].[Accounts](
    [AccountID] [uniqueidentifier] NOT NULL,
    [AccountNumber] [varchar](50) NOT NULL,
    [Name] [nvarchar](50) NOT NULL,
    [Balance] [decimal](10, 2) NOT NULL,
    [UserID] [uniqueidentifier] NOT NULL,
    [BankID] [int] NOT NULL,
    CONSTRAINT [PK__Accounts] PRIMARY KEY CLUSTERED ([AccountID] ASC)
);
GO

CREATE TABLE [dbo].[Bank](
    [BankID] [int] NOT NULL,
    [Name] [nvarchar](50) NOT NULL,
    CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED ([BankID] ASC)
);
GO

CREATE TABLE [dbo].[Transactions](
    [TransactionID] [uniqueidentifier] NOT NULL,
    [Type] [int] NOT NULL,
    [Amount] [decimal](10, 2) NOT NULL,
    [FromAccountID] [uniqueidentifier] NOT NULL,
    [ToAccountID] [uniqueidentifier] NULL,
    [CreatedAt] [datetime] NOT NULL,
    PRIMARY KEY CLUSTERED ([TransactionID] ASC)
);
GO

CREATE TABLE [dbo].[Users](
    [UserID] [uniqueidentifier] NOT NULL,
    [Name] [nvarchar](50) NOT NULL,
    [Username] [varchar](50) NOT NULL,
    [Password] [varchar](50) NOT NULL,
    CONSTRAINT [PK__Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);
GO

-- Insert initial data
PRINT 'Inserting initial data...';
INSERT [dbo].[Bank] ([BankID], [Name]) VALUES (1, N'กรุงไทย')
INSERT [dbo].[Bank] ([BankID], [Name]) VALUES (2, N'กสิกรไทย')
INSERT [dbo].[Bank] ([BankID], [Name]) VALUES (3, N'กรุงเทพ')
GO

INSERT [dbo].[Users] ([UserID], [Name], [Username], [Password]) VALUES (N'83f67598-0b94-461e-bd13-1d41264a14bb', N'Arthit', N'arthit2545', N'password')
INSERT [dbo].[Users] ([UserID], [Name], [Username], [Password]) VALUES (N'954824ac-a282-441e-867f-558d68b15233', N'Thunwa', N'thunwa2544', N'password')
INSERT [dbo].[Users] ([UserID], [Name], [Username], [Password]) VALUES (N'a5ce6dec-6743-4c31-8587-6a04738eb352', N'Nasan', N'nasan2544', N'password')
INSERT [dbo].[Users] ([UserID], [Name], [Username], [Password]) VALUES (N'd7e4a6a8-3c8a-473b-87b0-bc896589c465', N'Adsadawut', N'adsadawut2544', N'password')
GO

INSERT [dbo].[Accounts] ([AccountID], [AccountNumber], [Name], [Balance], [UserID], [BankID]) VALUES (N'f8723e1d-5b5e-417d-af2c-1a9ab9565221', N'1512584201', N'บัญชีคุณอาทิตย์', CAST(0.00 AS Decimal(10, 2)), N'83f67598-0b94-461e-bd13-1d41264a14bb', 1)
INSERT [dbo].[Accounts] ([AccountID], [AccountNumber], [Name], [Balance], [UserID], [BankID]) VALUES (N'0b24ebf1-c700-4237-b143-723f8ea35e86', N'9857501254', N'บัญชีคุณณสันต์', CAST(0.00 AS Decimal(10, 2)), N'a5ce6dec-6743-4c31-8587-6a04738eb352', 3)
INSERT [dbo].[Accounts] ([AccountID], [AccountNumber], [Name], [Balance], [UserID], [BankID]) VALUES (N'4c0950e0-c047-4d74-8c7e-a40ed1dc5703', N'1520145210', N'บัญชีคุณธันวา', CAST(0.00 AS Decimal(10, 2)), N'954824ac-a282-441e-867f-558d68b15233', 1)
INSERT [dbo].[Accounts] ([AccountID], [AccountNumber], [Name], [Balance], [UserID], [BankID]) VALUES (N'd82079da-8aa0-40af-aad5-af4bb4c0a97c', N'5852145012', N'บัญชีคุณอาทิตย์2', CAST(0.00 AS Decimal(10, 2)), N'83f67598-0b94-461e-bd13-1d41264a14bb', 2)
INSERT [dbo].[Accounts] ([AccountID], [AccountNumber], [Name], [Balance], [UserID], [BankID]) VALUES (N'8e61bce8-2d66-445a-a1cc-93befb5933da', N'8750124820', N'บัญชีคุณอัษฎาวุฒิ', CAST(0.00 AS Decimal(10, 2)), N'd7e4a6a8-3c8a-473b-87b0-bc896589c465', 1)
GO

-- Add foreign key constraints
PRINT 'Adding foreign key constraints...';
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [FK__Accounts__BankID] FOREIGN KEY([BankID]) REFERENCES [dbo].[Bank] ([BankID]);
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [FK__Accounts__UserID] FOREIGN KEY([UserID]) REFERENCES [dbo].[Users] ([UserID]);
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK__Transactions__FromAccountID] FOREIGN KEY([FromAccountID]) REFERENCES [dbo].[Accounts] ([AccountID]);
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK__Transactions__ToAccountID] FOREIGN KEY([ToAccountID]) REFERENCES [dbo].[Accounts] ([AccountID]);
PRINT 'Database setup completed.';